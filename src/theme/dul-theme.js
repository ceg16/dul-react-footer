import { createTheme } from '@mui/material/styles';

const dulTheme = createTheme({
  //Values taken from:
  //https://gitlab.oit.duke.edu/dul-its/dulcet/-/blob/develop/themes/custom/drupal9_dulcet/drupal9_dulcet.breakpoints.yml
  breakpoints: {
    values: {
      xs: 0,
      sm: 576,
      md: 768,
      lg: 992,
      xl: 1200,
    },
  },
  palette: {
    primary: {
      main: '#053482',
    },
    secondary: {
      main: '#dadada',
    },
    error: {
      main: '#c84e00',
    },
    warning: {
      main: '#ffd960',
    },
    info: {
      main: '#004ccc',
    },
    success: {
      main: '#a1b70d',
    },
  },
  typography: {
    fontFamily: [
      '"Source Sans Pro"',
      'Calibri',
      'Candara',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  components: {
    MuiLink: {
      styleOverrides: {
        root: {
          fontFamily: [
            '"Source Sans Pro"',
            'Calibri',
            'Candara',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
          ].join(','),
          fontWeight: 400,
          color: "#00539b",
        },
      },
    },
  },
});

export default dulTheme;