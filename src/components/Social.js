import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';

export default function Social(props) {

  const socialMediaIcons = props.social.icons.map((item) => (
    <Grid item key={item.altText}>
      <Link href={item.linkURL}>
        <img alt={item.altText} src={item.imageSource}></img>
      </Link>
    </Grid>
  ));

  const socialListItems = props.social.listItems.map((item) => (
    <ListItem key={item.linkText} component="li" disableGutters>
      <Link underline="hover" href={item.linkURL}>
        {item.linkText}
      </Link>
      {/* If list item has second link, add it inline after a forward slash */}
      {item.linkText2 &&
        <span>&nbsp;/&nbsp;</span>
      }
      {item.linkText2 &&
        <Link underline="hover" href={item.linkURL2}>
          {item.linkText2}
        </Link>
      }
    </ListItem >
  ));

  return (
    <Grid item lg={3} mb={4}>
      <Grid container direction="row" spacing={1} mb={.45}>
        {socialMediaIcons}
      </Grid>
      <Divider light />
      <List dense component="ul"
        sx={{
          fontSize: ".95rem"
        }}
      >
        {socialListItems}
      </List>
      <Link rel="license" href={props.social.license.linkURL}>
        <img
          alt={props.social.license.altText}
          src={props.social.license.imageSource}
          title={props.social.license.imageTitle}
        ></img>
      </Link>
    </Grid>
  );
}
