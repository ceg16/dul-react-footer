import Grid from '@mui/material/Grid';

export default function libraryLogo(props) {
  return (
    <Grid item lg={1} mb={4}>
      <img alt={props.libraryLogo.altText} src={props.libraryLogo.imageSource} />
    </Grid>
  );
}
