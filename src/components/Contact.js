import Divider from '@mui/material/Divider';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';

export default function Contact(props) {
  return (
    <Grid item lg={2} mb={4}>
      <Link component="h2" underline="hover" mb={1} href="//library.duke.edu/about/contact"
        sx={{
          fontSize: "1.2rem",
          fontWeight: "600"
        }}
      >
        {props.contact.header}
      </Link>
      <Divider light />
      <Typography component="address" py={1}
        sx={{
          fontStyle: "normal",
          fontSize: ".75rem"
        }}
      >
        {props.contact.addressLine1}<br></br>
        {props.contact.addressLine2}<br></br>
        {props.contact.addressLine3}<br></br>
        {props.contact.addressLine4}
      </Typography>
    </Grid>
  );
}
