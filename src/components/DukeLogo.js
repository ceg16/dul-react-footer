import Grid from '@mui/material/Grid';

export default function DukeLogo(props) {
  return (
    <Grid item lg={2} mb={4}>
      <img alt={props.dukeLogo.altText} src={props.dukeLogo.imageSource} />
    </Grid>
  );
}
