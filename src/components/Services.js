import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';

export default function Services(props) {

  // Split list items in half (for two column display):
  // https://flaviocopes.com/how-to-cut-array-half-javascript/
  const list = props.services.listItems;
  const half = Math.ceil(list.length / 2);
  const firstHalf = list.slice(0, half);
  const secondHalf = list.slice(half);

  //Map list items to markup
  const servicesColumn1 = firstHalf.map((item) => (
    <ListItem key={item.linkText} component="li" disableGutters>
      <Link underline="hover" href={item.linkURL}>
        {item.linkText}
      </Link>
    </ListItem>
  ));

  const servicesColumn2 = secondHalf.map((item) => (
    <ListItem key={item.linkText} component="li" disableGutters>
      <Link underline="hover" href={item.linkURL}>
        {item.linkText}
      </Link>
    </ListItem>
  ));

  return (
    <Grid item lg={4} mb={4}>
      <Link component="h2" underline="hover" mb={1} href="https://library.duke.edu/services"
        sx={{
          fontSize: "1.2rem",
          fontWeight: "600"
        }}
      >
        {props.services.header}
      </Link>
      <Divider light />
      <Grid container direction="row" spacing={2}
        sx={{
          fontSize: ".95rem"
        }}
      >
        <Grid item lg={6}>
          <List dense component="ul">
            {servicesColumn1}
          </List>
        </Grid>
        <Grid item lg={6}>
          <List dense component="ul">
            {servicesColumn2}
          </List>
        </Grid>
      </Grid>
    </Grid>
  );
}
