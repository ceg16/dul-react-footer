import React, { Component } from 'react';
import footer from './footerData.json';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import LibraryLogo from './components/LibraryLogo';
import Contact from './components/Contact';
import Services from './components/Services';
import Social from './components/Social';
import DukeLogo from './components/DukeLogo';

class App extends Component {

  state = footer;

  render() {
    return (
      <Container component="footer" maxWidth="xl"
        sx={{
          margin: "auto",
          maxWidth: "lg",
          height: "auto",
          backgroundColor: "#eee",
          color: "#222222",
          paddingTop: "1.5rem",
          paddingBottom: "1rem",
          borderTop: "6px solid #ddd",
          borderRadius: "0 0 10px 10px"
        }}
      >
        <Grid container direction="row" spacing={4}>
          <LibraryLogo
            libraryLogo={this.state.libraryLogo}
          />
          <Contact
            contact={this.state.contact}
          />
          <Services
            services={this.state.services}
          />
          <Social
            social={this.state.social}
          />
          <DukeLogo
            dukeLogo={this.state.dukeLogo}
          />
        </Grid>
      </Container>
    );
  }
}

export default App;
